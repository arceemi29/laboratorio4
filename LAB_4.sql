CREATE TABLESPACE DATA_DAT DATAFILE 'C:\ORACLEXE\APP\ORACLE\ORADATA\XE\DATA_DAT.DBF' SIZE 100M,
'C:\ORACLEXE\APP\ORACLE\ORADATA\XE\ DATA_DAT1.DBF' SIZE 50M,
'C:\ORACLEXE\APP\ORACLE\ORADATA\XE\ DATA_DAT2.DBF' SIZE 50M;

--Crear el usuario
CREATE USER "DATOS"
DEFAULT TABLESPACE "DATA_DAT"
TEMPORARY TABLESPACE "TEMP"
IDENTIFIED BY "Admin12345"
ACCOUNT UNLOCK;

ALTER USER "DATOS" QUOTA UNLIMITED ON "DATA_DAT";

--Dar permisos (ROLES)
GRANT "CONNECT" TO "DATOS";
GRANT "RESOURCE" TO "DATOS";

CREATE TABLE CLIENTES_TALLER(
    ID_CLIENTE INT NOT NULL PRIMARY KEY,
    CEDULA VARCHAR2(20) NOT NULL,
    APELLIDO1 VARCHAR2(20)NOT NULL,
    NOMBRE VARCHAR2(30)NOT NULL,
    DIRECCION VARCHAR2(20) NOT NULL,
    TELEDONO INT NOT NULL,
    CONSTRAINT UK_CEDULA UNIQUE (CEDULA)
);

CREATE TABLE COCHES_TALLER(
    ID_COCHE INT NOT NULL PRIMARY KEY,
    ID_CLIENTE INT REFERENCES CLIENTES_TALLER (ID_CLIENTE),
    MARCA VARCHAR2(30),
    MODELO VARCHAR2(30),
    COLOR VARCHAR2(30)
);

CREATE TABLE REPARACIONES(
    ID_REPARACION INT NOT NULL PRIMARY KEY,
    ID_COCHE INT REFERENCES COCHES_TALLER(ID_COCHE),
    DESCRIPCION VARCHAR2(250),
    FECHA_ENTRADA VARCHAR2(30),
    FECHA_SALIDA VARCHAR2(30),
    HORAS VARCHAR2(30)
);


CREATE TABLE PIEZAS(
    ID_PIEZAS INT NOT NULL PRIMARY KEY,
    DESCRIPCION VARCHAR2(250),
    PRECIO FLOAT,
    STOCK INT
);


CREATE TABLE DETA_REPARACION(
    ID_REPARACION INT REFERENCES REPARACIONES(ID_REPARACION),
    ID_PIEZAS INT REFERENCES PIEZAS(ID_PIEZAS),
    CANT_UNIDADES INT NOT NULL
);

---DATOS INSERTADOS 
--TABLA CLIENTES_TALLER
INSERT INTO CLIENTES_TALLER (ID_CLIENTE,CEDULA,APELLIDO1,NOMBRE,DIRECCION,TELEDONO)VALUES(1,'207650987', 'PEREZ', 'FERNANDA', 'BARRIO DEL CARMEN', 65458795);
INSERT INTO CLIENTES_TALLER (ID_CLIENTE,CEDULA,APELLIDO1,NOMBRE,DIRECCION,TELEDONO)VALUES(2,'102540487', 'ARROYO', 'MARIA', 'SANTA MARIA', 75824569);
INSERT INTO CLIENTES_TALLER (ID_CLIENTE,CEDULA,APELLIDO1,NOMBRE,DIRECCION,TELEDONO)VALUES(3,'302580164', 'PORRAS', 'CARLOS', 'BARRIO SAN ANTONIO', 87964521);
INSERT INTO CLIENTES_TALLER (ID_CLIENTE,CEDULA,APELLIDO1,NOMBRE,DIRECCION,TELEDONO)VALUES(4,'402130589', 'GUEVARA', 'DIEGO', 'SAN JOSE', 84216598);
INSERT INTO CLIENTES_TALLER (ID_CLIENTE,CEDULA,APELLIDO1,NOMBRE,DIRECCION,TELEDONO)VALUES(5,'504890365', 'MORETI', 'GABRIEL', 'BARRIO ARCOIRIS', 67892134);
INSERT INTO CLIENTES_TALLER (ID_CLIENTE,CEDULA,APELLIDO1,NOMBRE,DIRECCION,TELEDONO)VALUES(6,'604780325', 'GUTIERRES', 'RANDY', 'BARRIO SANTA FE', 87456125);
INSERT INTO CLIENTES_TALLER (ID_CLIENTE,CEDULA,APELLIDO1,NOMBRE,DIRECCION,TELEDONO)VALUES(7,'707650785', 'SOTO', 'FERNANDO', 'SANTA ROSA', 84562132);


---TABLA COCHES_TALLER
INSERT INTO COCHES_TALLER (ID_COCHE,ID_CLIENTE,MARCA,MODELO,COLOR)VALUES(1,1,'BMW', '5201', 'GRIS');
INSERT INTO COCHES_TALLER (ID_COCHE,ID_CLIENTE,MARCA,MODELO,COLOR)VALUES(2,2,'ABARTH', 'F595', 'NEGRO');
INSERT INTO COCHES_TALLER (ID_COCHE,ID_CLIENTE,MARCA,MODELO,COLOR)VALUES(3,3,'FERRARI', 'FERRARI812', 'ROJO');
INSERT INTO COCHES_TALLER (ID_COCHE,ID_CLIENTE,MARCA,MODELO,COLOR)VALUES(4,4,'BMW', 'BMWSERIAL1', 'AZUL');
INSERT INTO COCHES_TALLER (ID_COCHE,ID_CLIENTE,MARCA,MODELO,COLOR)VALUES(5,5,'DACIA', 'JOGGER', 'MARRON');
INSERT INTO COCHES_TALLER (ID_COCHE,ID_CLIENTE,MARCA,MODELO,COLOR)VALUES(6,6,'LEXUS', 'CT', 'BLANCO');
INSERT INTO COCHES_TALLER (ID_COCHE,ID_CLIENTE,MARCA,MODELO,COLOR)VALUES(7,7,'TESLA', 'MODEL Y', 'AZUL MARINO');

---TABLA REPARACIONES
INSERT INTO REPARACIONES(ID_REPARACION,ID_COCHE,DESCRIPCION,FECHA_ENTRADA,FECHA_SALIDA,HORAS)VALUES(1,1,'CARRO COLOR GRIS BMW', '5/08/2022', '6/08/2022', '4');
INSERT INTO REPARACIONES(ID_REPARACION,ID_COCHE,DESCRIPCION,FECHA_ENTRADA,FECHA_SALIDA,HORAS)VALUES(2,2,'CARRO COLOR NEGRO ABARTH' ,'4/08/2022', '5/08/2022', '2');
INSERT INTO REPARACIONES(ID_REPARACION,ID_COCHE,DESCRIPCION,FECHA_ENTRADA,FECHA_SALIDA,HORAS)VALUES(3,3,'CARRO COLOR ROJO FERRARI','1/08/2022', '3/08/2022', '48');
INSERT INTO REPARACIONES(ID_REPARACION,ID_COCHE,DESCRIPCION,FECHA_ENTRADA,FECHA_SALIDA,HORAS)VALUES(4,4,'CARRO COLOR AZUL BMW','29/07/2022', '30/07/2022', '24');
INSERT INTO REPARACIONES(ID_REPARACION,ID_COCHE,DESCRIPCION,FECHA_ENTRADA,FECHA_SALIDA,HORAS)VALUES(5,5,'CARRO COLOR MARRON DACIA','5/08/2022', '6/08/2022', '8');
INSERT INTO REPARACIONES(ID_REPARACION,ID_COCHE,DESCRIPCION,FECHA_ENTRADA,FECHA_SALIDA,HORAS)VALUES(6,6,'CARRO COLOR BLANCO LEXUS','1/08/2022', '2/08/2022', '3');
INSERT INTO REPARACIONES(ID_REPARACION,ID_COCHE,DESCRIPCION,FECHA_ENTRADA,FECHA_SALIDA,HORAS)VALUES(7,7,'CARRO COLOR AZUL MARINO TESLA','5/08/2022', '6/08/2022', '10');

----TABLA PIEZAS
INSERT INTO PIEZAS(ID_PIEZAS,DESCRIPCION,PRECIO,STOCK)VALUES(1,'MOTOR',20000, 15);
INSERT INTO PIEZAS(ID_PIEZAS,DESCRIPCION,PRECIO,STOCK)VALUES(2,'LLANTAS',10000, 25);
INSERT INTO PIEZAS(ID_PIEZAS,DESCRIPCION,PRECIO,STOCK)VALUES(3,'FRENOS',9000, 20);
INSERT INTO PIEZAS(ID_PIEZAS,DESCRIPCION,PRECIO,STOCK)VALUES(4,'PUESTAS',50000, 30);
INSERT INTO PIEZAS(ID_PIEZAS,DESCRIPCION,PRECIO,STOCK)VALUES(5,'LIQUIDO DE FRENO',1000000, 50);
INSERT INTO PIEZAS(ID_PIEZAS,DESCRIPCION,PRECIO,STOCK)VALUES(6,'VOLANTES',90000, 9);
INSERT INTO PIEZAS(ID_PIEZAS,DESCRIPCION,PRECIO,STOCK)VALUES(8,'FRENO DE MANOS',298300, 15);

--TABLA DETA_REPARACION
INSERT INTO DETA_REPARACION(ID_REPARACION,ID_PIEZAS,CANT_UNIDADES)VALUES(1,1,1);
INSERT INTO DETA_REPARACION(ID_REPARACION,ID_PIEZAS,CANT_UNIDADES)VALUES(2,2,2);
INSERT INTO DETA_REPARACION(ID_REPARACION,ID_PIEZAS,CANT_UNIDADES)VALUES(3,3,1);
INSERT INTO DETA_REPARACION(ID_REPARACION,ID_PIEZAS,CANT_UNIDADES)VALUES(4,4,4);
INSERT INTO DETA_REPARACION(ID_REPARACION,ID_PIEZAS,CANT_UNIDADES)VALUES(5,5,1);
INSERT INTO DETA_REPARACION(ID_REPARACION,ID_PIEZAS,CANT_UNIDADES)VALUES(6,6,1);
INSERT INTO DETA_REPARACION(ID_REPARACION,ID_PIEZAS,CANT_UNIDADES)VALUES(7,8,1);
INSERT INTO DETA_REPARACION(ID_REPARACION,ID_PIEZAS,CANT_UNIDADES)VALUES(2,2,1);


--VISTA #1
--PERMISO PARA PODER EJECUTAR VISTAS
GRANT CREATE VIEW TO DATOS
--VISTA QUE MUESTRA LAS REPARACIONES QUE SE HAN REALIZADO EN EL TALLER 
CREATE OR REPLACE VIEW VREPARACIONES AS 
SELECT CL.ID_CLIENTE,
(CL.NOMBRE ||' ' ||CL.APELLIDO1) NOMBRE_COMPLETO,
C.MARCA,
C.MODELO,
R.DESCRIPCION
FROM COCHES_TALLER C
LEFT JOIN CLIENTES_TALLER CL
ON C.ID_CLIENTE = CL.ID_CLIENTE
INNER JOIN REPARACIONES R
ON C.ID_COCHE = R.ID_COCHE

--VISTA#2
--VISTA QUE MUESTRA EL PRECIO QUE TIENE QUE PAGAR CADA CLIENTE POR CADA UNA DE LAS REPARACIONES 
CREATE OR REPLACE VIEW V_PRECIO AS 
SELECT (CL.NOMBRE ||' ' ||CL.APELLIDO1) NOMBRE_COMPLETO,
R.DESCRIPCION,
NVL(P.PRECIO,0) * NVL(DR.CANT_UNIDADES,0) PRECIO_PAGAR
FROM COCHES_TALLER C
LEFT JOIN CLIENTES_TALLER CL
ON C.ID_CLIENTE = CL.ID_CLIENTE
INNER JOIN REPARACIONES R
ON C.ID_COCHE = R.ID_COCHE
INNER JOIN DETA_REPARACION DR
ON R.ID_REPARACION = DR.ID_REPARACION
INNER JOIN PIEZAS P
ON DR.ID_PIEZAS = P.ID_PIEZAS


--FUNTION#1

--FUNCION QUE AGREGRE UN PORCENTAJE DE IMPUESTO 

--SIN EXCEPCION TODO BIEN 
CREATE OR REPLACE FUNCTION PRECIO_IVA (P_IVA NUMBER)
RETURN NUMBER 
    IS BEGIN 
        RETURN P_IVA+(P_IVA*0.13);
    END;


SELECT PRECIO_IVA(PRECIO) FROM PIEZAS

--CON EXCEPCION DA UN ERROR QUE NO LOGRE ENCONTRAR
CREATE OR REPLACE FUNCTION PRECIO_IVA (P_IVA NUMBER)
RETURN NUMBER 
    IS 
BEGIN 
    BEGIN
        RETURN P_IVA+(P_IVA*0.13);
        EXCEPTION 
        WHEN OTHERS THEN 
        RAISE_APPLICATION_ERROR(-20003, 'ALGO PASA');
    END;

--FUNTION#2

-- FUNCION QUE ME INDICA QUE SI LA REPARACION ES COSTOSA O ECONOMICO

--CON EXCEPCION DA UN ERROR QUE NO LOGRE ENCONTRAR
CREATE OR REPLACE FUNCTION F_VERIFICACION_COSTOS (P_COSTO NUMBER)
RETURN VARCHAR2
    IS
        VALIDACION VARCHAR2(30);
BEGIN
    BEGIN
        VALIDACION:='';
    IF P_COSTO <= 50000 THEN 
        VALIDACION:='ECONOMICO';
    ELSE VALIDACION:='COSTOSO';
    END IF;
    EXCEPTION 
    WHEN OTHERS THEN 
        RAISE_APPLICATION_ERROR(-20003, 'ALGO PASA');
RETURN VALIDACION;
END;

--SIN EXCEPCION TODO BIEN 
CREATE OR REPLACE FUNCTION F_VERIFICACION_COSTOS (P_COSTO NUMBER)
RETURN VARCHAR2
    IS
        VALIDACION VARCHAR2(30);
BEGIN
    VALIDACION:='';
    IF P_COSTO <= 50000 THEN 
        VALIDACION:='ECONOMICO';
    ELSE VALIDACION:='COSTOSO';
    END IF;
RETURN VALIDACION;
END;


SELECT F_VERIFICACION_COSTOS(PRECIO) FROM PIEZAS

--FUNCION#3

--BUSCA EL NOMBRE DEL CLIENTE QUE SE DESEA ENCONTRAR

--CON EXCEPCION DA UN ERROR QUE NO LOGRE ENCONTRAR
CREATE OR REPLACE FUNCTION F_ENCONTRAR_COCHE (P_COCHE NUMBER)
RETURN VARCHAR2 IS 

NOMBRE_FUNTION VARCHAR2(100);

BEGIN 
    BEGIN
         SELECT MARCA INTO NOMBRE_FUNTION FROM COCHES_TALLER
        WHERE ID_COCHE = P_COCHE;
        EXCEPTION
        WHEN OTHERS THEN 
        RAISE_APPLICATION_ERROR(-20003, 'ALGO PASA'); 
RETURN NOMBRE_FUNTION;
END;


--SIN EXCEPCION TODO BIEN 

CREATE OR REPLACE FUNCTION F_ENCONTRAR_COCHE (P_COCHE NUMBER)
RETURN VARCHAR2 IS 

NOMBRE_FUNTION VARCHAR2(100);

BEGIN 
        SELECT MARCA INTO NOMBRE_FUNTION FROM COCHES_TALLER
        WHERE ID_COCHE = P_COCHE;
RETURN NOMBRE_FUNTION;
END;

--PROCEDIMIENTOS 

--PROCEDIMIENTO#1

--PROSESO QUE MODIFICA EL PRECIO DEPENDIENDO LA MARCA QUE SE DESEE
CREATE OR REPLACE PROCEDURE AUMENTO_MARCA(MARCA IN VARCHAR2) AS
BEGIN 
UPDATE PIEZAS SET PRECIO=PRECIO+(PRECIO*0.8) 
WHERE MARCA = MARCA;
END;

--PROCEDIMIENTO#2

--INSERTA UN NUEVO REGISTRO EN LA TABLA DE COCHES_TALLER

CREATE OR REPLACE PROCEDURE AGREGAR_COCHE
(ID_COCHE IN NUMBER, ID_CLIENTE IN NUMBER , MARCA IN VARCHAR2 ,
MODELO IN VARCHAR2 ,COLOR IN VARCHAR2 ) AS
BEGIN 
INSERT INTO COCHES_TALLER VALUES (ID_COCHE,ID_CLIENTE,MARCA,MODELO,COLOR);
END;

--PROCEDIMIENTO#3
--PROCEDIMIENTO QUE ELIMINA UN VEHICULO DE LA TABLA COCHES_TALLER
CREATE OR REPLACE PROCEDURE ELIMINAR_COCHE (ID_COCHE IN NUMBER) IS
BEGIN 
DELETE FROM COCHES_TALLER WHERE ID_COCHE = ID_COCHE;
END;


---TRIGGER CLIENTES
--TRIGGER QUE EVITA QUE ESTA TABLA SEA MODIFICADA DE OTROS ESQUEMAS SOLO SYSTEM 
CREATE OR REPLACE TRIGGER SEGURIDAD_TABLA_CLIENTES
BEFORE INSERT OR UPDATE OR DELETE ON CLIENTES_TALLER
BEGIN
IF USER NOT IN ('SYSTEM','SYS') THEN
 RAISE_APPLICATION_ERROR(-20001,'Esta tabla no debe ser modificada bajo ninguna circunstancia');
 END IF;
END SEGURIDAD_TABLA_CLIENTES;





